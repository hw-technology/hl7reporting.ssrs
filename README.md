# README #

**hl7reporting.SSRS**

### What is this repository for? ###

* HL7 Reports for clients
* v1.0

### How do I get set up? ###

* Install latest version of SSDT-BI (or SSDT for SQL 2016 and lower)
* Configuration
* SSDT-BI
* Set project properties to deploy to a server other than http://plb-txx-di01.test-products.local/reportserver

### Contribution guidelines ###

* Before making significant changes, create a branch
* Make changes in branch, commit and push, then issue a pull request
* After review/approval, merge the branch into master.

### Who do I talk to? ###

* Data Operations